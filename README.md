## Prometheus Node Exporter Textfile Collectors

Downlods, installs and configure systemd for textfile collectors

Tested on debian 10.

## Role parameters

| name                                | type   | optionnal | default value                                                                          | description                                  |
| ------------------------------------|--------|-----------|----------------------------------------------------------------------------------------|----------------------------------------------|
| textfile_collector_version          | string | yes       | master                                                                                 | version to fetch from textfile_collector_git |
| textfile_collector_git              | string | yes       | "https://github.com/prometheus-community/node-exporter-textfile-collector-scripts.git" | git repo for textfile_collector              |
| textfile_collector_directory        | string | yes       | "/var/lib/node_exporter"                                                               | this directory is shared withe the node exporter, it will be writen by the collectors|
| textfile_collector_script_directory | string | yes       | "/usr/share/node-exporter-textfile-collectors"                                         | directory containing the collectors scripts  |
| textfile_collector_scripts          | string | yes       | [ "apt.sh", "ipmitool", "mellanox_hca_temp", "smartmon.sh" ]                           | list of the scripts installed to the machine |
| textfile_collector_services         | string | yes       | see [defaults/main.yml](defaults/main.yml)                                             | contains the systemd services definitions    |

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/prometheus-node-exporter-textfile-collectors.git
  scm: git
  version: v1.0
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: prometheus-node-exporter-textfile-collectors
      textfile_collector_version: master
      textfile_collector_git: "https://github.com/prometheus-community/node-exporter-textfile-collector-scripts.git"
      textfile_collector_directory: "/var/lib/node_exporter"
      textfile_collector_script_directory: "/usr/share/node-exporter-textfile-collectors"
      textfile_collector_scripts:
        - apt.sh
        - ipmitool
        - mellanox_hca_temp
        - smartmon.sh
      textfile_collector_services:
        apt:
          script: apt.sh
          Timer:
            - OnUnitActiveSec: "15min"  
        ipmitool-sensor:
          script: ipmitool
          Unit:
            - ConditionFileIsExecutable: "/usr/bin/ipmitool"
            - ConditionDirectoryNotEmpty: "/sys/class/ipmi"
          Timer:
            - OnUnitActiveSec: "1min"  
        mellanox-hca-temp:
          script: mellanox_hca_temp
          Unit:
            - ConditionFileIsExecutable: "/usr/bin/mget_temp_ext"
            - ConditionDirectoryNotEmpty: "/sys/class/infiniband"
          Timer:
            - OnUnitActiveSec: "1min"
        smartmon:
          script: smartmon.sh
          Unit:
            - ConditionPathExists: '/usr/sbin/smartctl'
            - ConditionPathExistsGlob: '|/dev/sd*'
            - ConditionPathExistsGlob: '|/dev/hd*'
          Timer:
            - OnUnitActiveSec: '15min'
```

## Tests

[tests/tests_prometheus_haproxy_exporter](tests/tests_prometheus_haproxy_exporter)
